package com.ionldp.markets;

import com.ionldp.api.FeeCalculator;
import com.ionldp.api.Trade;
import com.ionldp.api.trade.AccountType;
import com.ionldp.api.trade.ProductType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LMEFeeCalculatorTest {

    private Trade trade;

    @Before
    public void setup(){
        trade = new MockTrade();
    }

    @Test
    public void lmeFeeCalculator() throws Exception {
        FeeCalculator feeCalculator = new LMEFeeCalculator();
        double fee = feeCalculator.calculateFee(trade);
        Assert.assertEquals(10,fee,0.0);
    }

    public class MockTrade implements Trade {

        @Override
        public AccountType getClient() {
            return null;
        }

        @Override
        public void setClient(AccountType client) {

        }

        @Override
        public ProductType getProductType() {
            return null;
        }

        @Override
        public int getQuantity() {
            return 10;
        }

        @Override
        public void setQuantity(int quantity) {

        }

        @Override
        public double getFees() {
            return 0;
        }

        @Override
        public void setFees(double fees) {

        }

        @Override
        public String getExchange() {
            return null;
        }

        @Override
        public void setExchange(String exchange) {

        }
    }
}
