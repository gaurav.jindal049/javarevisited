package com.ionldp.rules;

import com.ionldp.api.FeeCalculator;
import com.ionldp.api.RuleBag;
import com.ionldp.api.Trade;
import com.ionldp.markets.LMEFeeCalculator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class BaseRuleBagTest {
    private RuleBag ruleBag;
    private final String EXCHANGE = "LME";
    private final FeeCalculator feeCalculator = new LMEFeeCalculator();
    private Trade trade;

    @Before
    public void setUp(){
        ruleBag = new BaseRuleBag();
        trade = Mockito.mock(Trade.class);
    }

    @Test
    public void testBaseRuleBagRegisterAndGetMethod(){
        when(trade.getExchange()).thenReturn(EXCHANGE);
        ruleBag.register(EXCHANGE,feeCalculator);
        Assert.assertEquals(feeCalculator,ruleBag.get(trade));
    }

}
