package com.ionldp.products;

import com.ionldp.api.Trade;
import com.ionldp.api.trade.*;

public class BaseTrade implements Trade{
	private String exchange;
	private AccountType account;
	private int quantity;
	private double fees;
	
	public BaseTrade(String exchange, AccountType client, int quantity) {
		super();
		this.exchange = exchange;
		this.account = client;
		this.quantity = quantity;
	}
	
	public AccountType getClient() {
		return account;
	}
	public void setClient(AccountType client) {
		this.account = client;
	}
	public ProductType getProductType() {
		return ProductType.UNKNOWN;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getFees() {
		return fees;
	}
	public void setFees(double fees) {
		this.fees = fees;
	}
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	
}

