package com.ionldp.products;

import com.ionldp.api.trade.*;

public class OptionTrade extends BaseTrade{

	public OptionTrade(String exchange, AccountType account, int quantity) {
		super(exchange, account, quantity);
	}

	public ProductType getProductType() {
		return ProductType.OPTION;
	}

}
