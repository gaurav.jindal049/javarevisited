package com.ionldp.products;

import com.ionldp.api.trade.AccountType;
import com.ionldp.api.trade.ProductType;

public class FutureTrade extends BaseTrade {

	public FutureTrade(String exchange, AccountType account, int quantity) {
		super(exchange, account, quantity);
	}
	public ProductType getProductType() {
		return ProductType.FUTURE;
	}


}
