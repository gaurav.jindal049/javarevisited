package com.ionldp.products;

import com.ionldp.api.trade.AccountType;
import com.ionldp.api.trade.ProductType;


public class ForwardTrade extends BaseTrade{

	public ForwardTrade(String exchange, AccountType account, int quantity) {
		super(exchange, account, quantity);
	}
	public ProductType getProductType() {
		return ProductType.FORWARD;
	}


}
