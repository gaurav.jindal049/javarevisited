package com.ionldp.startup;

import java.io.IOException;

import com.ion.ldp.persistence.DataOperations;
import com.ion.ldp.persistence.FileDataOperations;
import com.ionldp.api.FeeCalculator;
import com.ionldp.api.RuleBag;
import com.ionldp.api.Trade;
import com.ionldp.api.trade.AccountType;
import com.ionldp.api.trade.ProductType;
import com.ionldp.markets.AbstractFeeCalculator;
import com.ionldp.markets.BSEFeeCalculator;
import com.ionldp.markets.CMEFeeCalculator;
import com.ionldp.markets.LMEFeeCalculator;
import com.ionldp.processor.BaseTradeProcessor;
import com.ionldp.processor.TradeFactory;
import com.ionldp.processor.TradeProcessor;
import com.ionldp.rules.BaseRuleBag;

/**
 * args
 * args[0]: Exchange Type
 * args[1]: Trade Type
 * args[2]: Acc type
 * args[3]: qty
 * ***/
public class Main {
	
	public static final String DEFAULT_FEE="fee.txt";
	
	public static final String DEFAULT_TOTAL_FEE="totalFee.txt";
	
	public static void main(String[] args) {


		String exchangeType = args[0];

		String tradeType = args[1];
		String accType = args[2];
		String qty = args[3];
		
		Double tradeFee=0d;
		
		DataOperations dao = new FileDataOperations();
		TradeProcessor processor = new BaseTradeProcessor(dao);
		
		RuleBag rulebag = new BaseRuleBag();
		rulebag.register("LME", new LMEFeeCalculator());
		rulebag.register("CME", new CMEFeeCalculator());
		rulebag.register("BSE", new BSEFeeCalculator());
		
		Trade trade = TradeFactory.getTrade(
					ProductType.ofName(tradeType), 
					exchangeType,
					AccountType.ofName(accType), 
					Integer.valueOf(qty)
				);
		
		try {
			tradeFee = processor.process(rulebag,trade);
		} catch (Exception e) {
			System.out.println("Invalid Trade, Fee can't be calculated");
			e.printStackTrace();
		}
		
		try {
			processor.write(tradeFee.toString(), DEFAULT_FEE, false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Double totalFee = 0.0;
		try {
			totalFee = processor.calculateTotalFee();
		} catch (IOException e) {
			System.out.println("Fee can't be written");
			e.printStackTrace();
		}
		
		try {
			processor.write(totalFee.toString(), DEFAULT_TOTAL_FEE, false);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}
