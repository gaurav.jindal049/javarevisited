package com.ionldp.markets;

import com.ionldp.api.Trade;
import com.ionldp.api.trade.ProductType;

public class BSEFeeCalculator extends AbstractFeeCalculator {

	@Override
	protected double calculateFeeExt(Trade trade) {
		// TODO : Fix This
		if (ProductType.FORWARD.equals(trade.getProductType())) {
			return 0.5;
		
		} else {
			if(trade.getQuantity()<=100) 
				return 0.2*trade.getQuantity();
			else
				return 0.18*trade.getQuantity();
			
		}
		
	}
}
