package com.ionldp.markets;

import com.ionldp.api.FeeCalculator;
import com.ionldp.api.Trade;

public  abstract class AbstractFeeCalculator implements FeeCalculator {

	@Override
	public final double calculateFee(Trade trade) throws Exception {
		
		if(!validateTrade(trade)) {
			throw new Exception("Invalid quantity");
		}
		
		double fee  = calculateFeeExt(trade);
		System.out.println(" " + fee + " ");
		return fee;
	}

	private boolean validateTrade(Trade trade) {
		
		if(trade.getQuantity()>0 && trade.getQuantity()<1000){
			return true;
		}
		else
		{
			System.out.println(" Invalid Trade");
			return false;
		}
	}
	
	protected abstract double calculateFeeExt(Trade trade) ;

}
