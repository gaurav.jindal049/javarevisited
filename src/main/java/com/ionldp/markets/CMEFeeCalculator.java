package com.ionldp.markets;

import com.ionldp.api.Trade;
import com.ionldp.api.trade.ProductType;
import com.ionldp.products.ForwardTrade;

public class CMEFeeCalculator extends AbstractFeeCalculator {

	@Override
	protected double calculateFeeExt(Trade trade) {


		if (ProductType.FORWARD.equals(trade.getProductType())) {
			return 0;
		} else if (ProductType.OPTION.equals(trade.getProductType())) {
			return 2;
		} else {
			return 0d;
		}
	}

}
