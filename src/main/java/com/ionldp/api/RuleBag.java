package com.ionldp.api;

public interface RuleBag {
	public void register(String exchangeName, FeeCalculator feesCalculator);
	public FeeCalculator get(Trade trade);
}
