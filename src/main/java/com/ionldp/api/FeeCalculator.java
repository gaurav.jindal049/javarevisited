package com.ionldp.api;

public interface FeeCalculator {
	public double calculateFee(Trade trade) throws Exception;
}
