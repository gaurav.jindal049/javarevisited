package com.ionldp.api.trade;

public enum AccountType {
	ELITE("elite"),PRIME("prime"),REGULAR("regular");
	
	private String type;
	
	private AccountType(String type){
		this.type=type;
	}
	
	public static AccountType ofName(String name){
		AccountType[] accountTypes=AccountType.values();
		
		for(AccountType accType:accountTypes){
			if(accType.getType().equalsIgnoreCase(name)){
				return accType;
			}
		}
		return null;
	}

	public String getType() {
		return type;
	}
}
