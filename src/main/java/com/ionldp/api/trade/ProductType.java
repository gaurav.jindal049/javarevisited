package com.ionldp.api.trade;

public enum ProductType {
	FUTURE("future"), FORWARD("forward"), OPTION("option"),UNKNOWN("unknown");

	private String type;

	private ProductType(String type) {
		this.type = type;
	}

	public static ProductType ofName(String name) {
		ProductType[] productTypes = ProductType.values();

		for (ProductType productType : productTypes) {
			if (productType.getType().equalsIgnoreCase(name)) {
				return productType;
			}
		}
		return ProductType.UNKNOWN;
	}

	public String getType() {
		return type;
	}
}
