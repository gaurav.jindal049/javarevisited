package com.ionldp.api;

import com.ionldp.api.trade.*;

public interface Trade {
	
	public AccountType getClient() ;
	
	public void setClient(AccountType client) ;
	
	public ProductType getProductType();
	
	public int getQuantity() ;
	
	public void setQuantity(int quantity) ;
	
	public double getFees() ;
	
	public void setFees(double fees) ;
	
	public String getExchange() ;
	
	public void setExchange(String exchange) ;
	
}