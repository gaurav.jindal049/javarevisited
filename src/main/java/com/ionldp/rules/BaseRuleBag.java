package com.ionldp.rules;

import java.util.HashMap;
import java.util.Map;
import com.ionldp.api.FeeCalculator;
import com.ionldp.api.RuleBag;
import com.ionldp.api.Trade;

public class BaseRuleBag implements RuleBag{
	Map<String, FeeCalculator> rulebag = new HashMap<>();
	
	public void register(String exchangeName, FeeCalculator feesCalculator) {
		rulebag.put(exchangeName, feesCalculator);
	}
	
	public FeeCalculator get(Trade trade)
	{
		return rulebag.get(trade.getExchange());
	}
}
