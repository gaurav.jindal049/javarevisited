/**
 * 
 */
package com.ionldp.processor;

import java.io.IOException;

import com.ion.ldp.persistence.DataOperations;
import com.ionldp.api.FeeCalculator;
import com.ionldp.api.RuleBag;
import com.ionldp.api.Trade;
import com.ionldp.markets.AbstractFeeCalculator;

/**
 * @author bhavik.banchpalliwar
 *
 */
public class BaseTradeProcessor implements TradeProcessor {

	private DataOperations dataOps;

	public BaseTradeProcessor(DataOperations dataOps) {
		this.dataOps = dataOps;
	}

	@Override
	public void write(String data, String filePath, boolean isAppend) throws IOException {
		// TODO logic to write the data to a file

	}

	@Override
	public double process(RuleBag rulebag, Trade trade) throws Exception {
		// TODO process to calculate fee of a trade
		return 0d;
	}

	@Override
	public double calculateTotalFee() throws IOException {
		// TODO calculate total fee. Iterate over all the fees captured in fee.txt
		//  and cummulate them to get the total fee
		return 0d;

	}
}
