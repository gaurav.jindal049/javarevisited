/**
 * 
 */
package com.ionldp.processor;

import com.ionldp.api.Trade;
import com.ionldp.api.trade.AccountType;
import com.ionldp.api.trade.ProductType;
import com.ionldp.products.BaseTrade;
import com.ionldp.products.ForwardTrade;
import com.ionldp.products.FutureTrade;
import com.ionldp.products.OptionTrade;

/**
 * @author bhavik.banchpalliwar
 *
 */
public class TradeFactory {
	
	public static Trade getTrade(ProductType productType,String exchange, AccountType account, int quantity){

		switch(productType){
			case FORWARD:
				return new ForwardTrade(exchange,account,quantity);
			case OPTION:
				return new OptionTrade(exchange,account,quantity);
			case FUTURE:
				return new FutureTrade(exchange,account,quantity);
			default:
				return new BaseTrade(exchange,account,quantity);

		}
	}

}
