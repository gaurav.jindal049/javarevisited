package com.ionldp.processor;

import java.io.IOException;

import com.ionldp.api.RuleBag;
import com.ionldp.api.Trade;

public interface TradeProcessor {
	
	double process(RuleBag rulebag,Trade tradeObj) throws Exception;
	
	double calculateTotalFee() throws IOException;
	
	void write(String data,String filePath,boolean isAppend) throws IOException ;

}
