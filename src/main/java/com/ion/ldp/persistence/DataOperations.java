/**
 * 
 */
package com.ion.ldp.persistence;

import java.io.IOException;
import java.util.List;

/**
 * @author bhavik.banchpalliwar
 *
 */
public interface DataOperations {
	
	void write(String tableName,String data,boolean isAppend) throws IOException;
	
	List<String> read(String tableName) throws IOException;

}
